# ZebDetFin

Refactored 'The Knight's Tour' problem. Check 'config/config.yaml' file & setup application:
* Switch to 'DFS' default or 'Warendorf's' mode
* Log dead ends
* ...

### Run code

```shell
# list make commands
make

# run program
make run
```

### Authors and acknowledgment
ljudevit.ocic@gmail.com

### License
MIT

### Project status
Done with 0% test coverage.
