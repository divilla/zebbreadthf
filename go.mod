module gitlab.com/divilla/zebdetfin

go 1.17

require (
	go.uber.org/zap v1.21.0
	golang.org/x/text v0.3.7
	gopkg.in/yaml.v3 v3.0.1
)

require (
	go.uber.org/atomic v1.9.0 // indirect
	go.uber.org/multierr v1.8.0 // indirect
)
