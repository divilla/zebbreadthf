package graph

import (
	"gitlab.com/divilla/zebdetfin/internal/config"
	"go.uber.org/zap"
)

type (
	Graph struct {
		logger *zap.Logger
		// neighbours are de facto map of position index & neighbour node's position indexes
		neighbours [][]int
		// edges are slice of all legal moves represented as array of starting & ending node position index
		edges           [][2]int
		width           int
		height          int
		size            int
		startPos        int
		startPosCounter int
		maxDegree       int
		moves           [][2]int
		posX            []string
		posY            []string
		tick            int
		counter         int
		logFailed       bool
	}
)

func NewGraph(log *zap.Logger, cfg *config.Config) *Graph {
	size := cfg.BoardSize[0] * cfg.BoardSize[1]

	return &Graph{
		logger:     log,
		neighbours: make([][]int, size),
		width:      cfg.BoardSize[0],
		height:     cfg.BoardSize[1],
		size:       size,
		startPos:   cfg.StartingPosition[0] + cfg.BoardSize[0]*cfg.StartingPosition[1],
		maxDegree:  len(cfg.Moves),
		moves:      cfg.Moves,
		posX:       []string{"A", "B", "C", "D", "E", "F", "G", "H", "I", "J"},
		posY:       []string{"1", "2", "3", "4", "5", "6", "7", "8", "9", "0"},
		tick:       cfg.TickSeconds,
		logFailed:  cfg.LogFailed,
	}
}

func (g *Graph) Init() {
	for y := 0; y < g.height; y++ {
		for x := 0; x < g.width; x++ {
			i := g.positionToIndex(x, y)
			lm := g.legalMoves(x, y)
			g.neighbours[i] = lm
			for _, m := range lm {
				g.edges = append(g.edges, [2]int{i, m})
			}
		}
	}
}

func (g Graph) Size() int {
	return g.size
}

func (g *Graph) NextStartPos() (int, bool) {
	sp := g.startPos
	g.startPosCounter++

	return (sp + g.startPosCounter) % g.size, g.startPosCounter == g.size
}

func (g Graph) MaxDegree() int {
	return g.maxDegree
}

func (g Graph) GetNeighbours(i int) []int {
	return g.neighbours[i]
}

func (g Graph) TotalEdges() int {
	return len(g.edges)
}

func (g *Graph) Inc() {
	g.counter++
}

func (g Graph) Counter() int {
	return g.counter
}

func (g Graph) Tick() int {
	return g.tick
}

func (g Graph) LogFailed() bool {
	return g.logFailed
}

func (g Graph) legalMoves(x, y int) []int {
	var lm []int
	for _, p := range g.moves {
		nx := x + p[0]
		ny := y + p[1]
		if g.isOnBoard(nx, ny) {
			lm = append(lm, g.positionToIndex(nx, ny))
		}
	}

	return lm
}

func (g Graph) isOnBoard(x, y int) bool {
	return x >= 0 && x < g.width && y >= 0 && y < g.height
}

func (g Graph) indexToPosition(i int) (x int, y int) {
	x = i % g.size % g.width
	y = i % g.size / g.width
	return
}

func (g Graph) positionToIndex(x, y int) int {
	return x + y*g.width
}
