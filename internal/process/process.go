package process

import (
	"fmt"
	"gitlab.com/divilla/zebdetfin/pkg/graph"
)

func DFS(g *graph.Graph) ([]int, bool) {
	for {
		var path []int
		used := make([]bool, g.Size())
		sp, stop := g.NextStartPos()
		if stop {
			return path, false
		}

		if _, solved := dfs(g, 1, sp, used, path); solved {
			fmt.Println()
		}
	}
}

func dfs(g *graph.Graph, moveNumber, index int, used []bool, path []int) ([]int, bool) {
	newUsed := setUsedImmutable(index, used)
	newPath := appendIntSliceImmutable(path, index)

	if moveNumber == g.Size() {
		return newPath, true
	}

	var success bool
	for _, i := range g.GetNeighbours(index) {
		if !used[i] {
			if out, ok := dfs(g, moveNumber+1, i, newUsed, newPath); ok {
				success = true
				g.Inc()
				fmt.Println("\r\n----------------  Solved  -------------------")
				fmt.Println(out)
				fmt.Println("---------------------------------------------")
				fmt.Println()
			}
		}
	}

	g.Inc()
	if g.LogFailed() && !success {
		fmt.Println("----------------  Failed  -------------------")
		fmt.Println(moveNumber, newPath)
	}

	return newPath, false
}

func Warnsdorff(g *graph.Graph) ([]int, bool) {
	for {
		var path []int
		used := make([]bool, g.Size())
		sp, stop := g.NextStartPos()
		if stop {
			return path, false
		}

		if _, solved := warnsdorff(g, 1, sp, used, path); solved {
			fmt.Println()
		}
	}
}

func warnsdorff(g *graph.Graph, moveNumber, index int, used []bool, path []int) ([]int, bool) {
	newUsed := setUsedImmutable(index, used)
	newPath := appendIntSliceImmutable(path, index)

	if len(newPath) == g.Size() {
		return newPath, true
	}

	var minDegreeIndexes []int
	minDegree := g.MaxDegree()
	for _, i := range g.GetNeighbours(index) {
		if newUsed[i] {
			continue
		}

		degree := 0
		for _, ni := range g.GetNeighbours(i) {
			if !newUsed[ni] {
				degree++
			}
		}
		if degree == 0 && moveNumber < g.Size()-1 {
			continue
		}

		if degree < minDegree {
			minDegree = degree
			minDegreeIndexes = []int{i}
		} else if degree == minDegree {
			minDegreeIndexes = append(minDegreeIndexes, i)
		}
	}

	var success bool
	for _, i := range minDegreeIndexes {
		if out, ok := warnsdorff(g, moveNumber+1, i, newUsed, newPath); ok {
			success = true
			g.Inc()
			fmt.Println("\n----------------  Solved  -------------------")
			fmt.Println(out)
			fmt.Println("---------------------------------------------")
			fmt.Println()
		}
	}

	g.Inc()
	if g.LogFailed() && !success {
		fmt.Println("----------------  Failed  -------------------")
		fmt.Println(moveNumber, newPath)
	}

	return newPath, false
}

func appendIntSliceImmutable(in []int, elems ...int) []int {
	lIn := len(in)
	out := make([]int, lIn+len(elems))
	for k, v := range in {
		out[k] = v
	}
	for k, v := range elems {
		out[k+lIn] = v
	}

	return out
}

func setUsedImmutable(index int, in []bool) []bool {
	out := make([]bool, len(in))
	for k, v := range in {
		if k == index {
			out[k] = true
		} else {
			out[k] = v
		}
	}

	return out
}
